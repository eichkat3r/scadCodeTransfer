# scadCodeTransfer

A little script I wrote for an OpenSCAD Workshop to use OpenSCAD in a slide like manner. 

I do not plan to further develope this script. 
If I get some requests a may add the possibility to us alternative file locations for in and output but thats all i am 
willed to do on additional developement right now.

## How to use this:

open output.scad in openscad activate "Automatic reload and Preview" and start the script.

This litle script allows to use OpenSCAD as a sort of presentation software.

On start it will try to load input.scad.
If this file exists it will start to copy it's contents to output.scad up to the first line being equal to
//-- or //--//
if this line is //--
and you press enter the programm will continue 
to copy the file up to the next line of one of these types (excluding the //-- line)
if the line is //--//
it will erase the content of output.scad and than 
copies the next lines till the occurance of a //-- or //--// line

Pressing 'u' + enter will erase the content till the last occurence of //--
or if the lattest occurance of //-- or //--// is of sort //--//
it will delete the content of output.scad jump to the second lattest
//--// line and starts copying again.

## more structured description

If you think of the input.scad file as a slideshow

a line being equal to //-- 
marks the begin of a new slide item.
//--// marks the begin of e new slide.

pressing enter loads the next item of the slide or shows the next slide if there are no items left.

Pressing u+enter (in sequence not simeltaniously) will delete the last item or
if there is only one item left it will restart the preceeding slide.

